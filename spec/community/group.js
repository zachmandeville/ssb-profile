const { string, image, poBoxId, boolean } = require('../lib/field-types')
const { isGroupEncrypted, hasValidAliveInterval } = require('../lib/validators')
const GetTransformation = require('../lib/get-transformation')

const privateCommunitySpec = {
  type: 'profile/community',
  props: {
    preferredName: string,
    description: string,

    address: string,
    city: string,
    country: string,
    postCode: string,
    phone: string,
    email: string,

    poBoxId,

    avatarImage: image,
    headerImage: image,

    // settings
    allowWhakapapaViews: boolean,
    allowPersonsList: boolean,
    allowStories: boolean
  },
  hooks: {
    isRoot: [isGroupEncrypted, hasValidAliveInterval],
    isUpdate: [isGroupEncrypted, hasValidAliveInterval]
  }
}
privateCommunitySpec.getTransformation = GetTransformation(privateCommunitySpec)

module.exports = privateCommunitySpec
