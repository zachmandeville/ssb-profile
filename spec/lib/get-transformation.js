module.exports = function GetTransformation (spec) {
  const valid = [
    ...Object.keys(spec.staticProps || {}),
    ...Object.keys(spec.props || {}),
    'tombstone', // prop added by ssb-crut
    'authors' //    prop added by ssb-crut-authors
  ]

  const allowedContentFields = new Set(valid)
  allowedContentFields.add('type')
  allowedContentFields.add('tangles')
  allowedContentFields.add('recps')

  return function getTransformation (m) {
    /* SAFE MUTATION ? */
    const content = m.value.content
    Object.keys(content).forEach(key => {
      if (!allowedContentFields.has(key)) delete content[key]
    })

    return content

    /* NO MUTATION */
    // return valid.reduce((acc, key) => {
    //   if (m.value.content[key]) acc[key] = m.value.content[key]

    //   return acc
    // }, {})
  }
}
