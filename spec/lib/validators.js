const edtf = require('edtf')
const get = require('lodash.get')
const Validator = require('is-my-json-valid')
const { cloakedMsgIdRegex: groupIdRegex } = require('ssb-ref')

function isPublic (content) {
  if (!content.recps) return true

  return new Error(`A public ${content.type} is not allowed recps`)
}

const schema = {
  group: {
    type: 'string',
    pattern: groupIdRegex.toString().slice(1, -1)
  },
  poBoxId: {
    type: 'string',
    pattern: '^ssb:identity/po-box/'
  },
  feedId: {
    type: 'string',
    pattern: '^(@|ssb:feed/)[^\\s]+' // feedIdPattern TODO - improve
  }
}
const isGroupRecps = Validator(
  {
    type: 'array',
    items: schema.group,
    minItems: 1,
    maxItems: 1
  },
  { verbose: true }
)
function isGroupEncrypted (content) {
  if (isGroupRecps(content.recps)) return true

  return new Error(`A private ${content.type} must be encrypted only to a group`)
}

// can be:
// - EITHER [poBoxId, feedId], [poBoxId] for people sending into admins
// - OR [groupId] for admins publishing
const isKaitiakiRecps = Validator(
  {
    oneOf: [
      {
        type: 'array',
        items: [
          schema.poBoxId,
          schema.feedId
        ],
        minItems: 1,
        maxItems: 2
      },
      {
        type: 'array',
        items: schema.group,
        minItems: 1,
        maxItems: 1
      }
    ]
  },
  { verbose: true }
)
function isKaitiakiEncrypted (content) {
  if (isKaitiakiRecps(content.recps)) return true

  return new Error(`A private ${content.type} must be encrypted to a group, or a groups poBox`)
}

function HasValidDateInterval (field) {
  return function hasValidDateInterval (content) {
    if (!content[field]) return true
    if (!content[field].set) return true

    try {
      const result = edtf.parse(content[field].set)
      if (result.type !== 'Interval') return new Error(`profile expected ${field} to be an EDTF Interval, but got type ${result.type}`)
      return true
    } catch (e) {
      return new Error(`profile expects ${field} to be set to an EDTF compatible interval, got ${content[field].set}`)
    }
  }
}

function HasValidDateCustomFields (content) {
  if (!content) return true
  if (!content.customFields) return true

  return Object.entries(content.customFields)
    .filter(([_, date]) => get(date, 'type') === 'date')
    .every(([key, date]) => {
      try {
        edtf.parse(date.value)
        return true
      } catch (err) {
        return false
      }
    })
}

module.exports = {
  isPublic,
  isGroupEncrypted,
  isKaitiakiEncrypted,
  hasValidAliveInterval: HasValidDateInterval('aliveInterval'),
  hasValidDateCustomFields: HasValidDateCustomFields
}
