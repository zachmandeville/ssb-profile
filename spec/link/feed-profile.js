module.exports = {
  type: 'link/feed-profile',
  tangle: 'link',
  staticProps: {
    parent: { $ref: '#/definitions/feedId', required: true },
    child: { $ref: '#/definitions/messageId', required: true }
  },
  props: {}
}
