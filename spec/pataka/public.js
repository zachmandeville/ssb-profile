const { string, image } = require('../lib/field-types')
const { isPublic } = require('../lib/validators')
const GetTransformation = require('../lib/get-transformation')

const publicPatakaSpec = {
  type: 'profile/pataka',
  props: {
    preferredName: string,
    description: string,

    avatarImage: image,

    phone: string,
    email: string
  },
  hooks: {
    isRoot: [isPublic],
    isUpdate: [isPublic]
  }
}
publicPatakaSpec.getTransformation = GetTransformation(publicPatakaSpec)

module.exports = publicPatakaSpec
