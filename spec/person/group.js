const SimpleSet = require('@tangle/simple-set')

const { string, gender, number, boolean, image, stringArray, customFields } = require('../lib/field-types')
const { isGroupEncrypted, hasValidAliveInterval, hasValidDateCustomFields } = require('../lib/validators')
const GetTransformation = require('../lib/get-transformation')

const personGroupSpec = {
  type: 'profile/person',
  props: {
    preferredName: string,
    legalName: string,
    altNames: SimpleSet(),

    avatarImage: image,
    headerImage: image,

    gender,
    description: string,

    aliveInterval: string,
    deceased: boolean,
    placeOfBirth: string,
    placeOfDeath: string,
    buriedLocation: string,
    birthOrder: number,

    city: string,
    country: string,
    postCode: string,

    profession: string,
    education: stringArray,
    school: stringArray,

    customFields
  },
  hooks: {
    isRoot: [isGroupEncrypted, hasValidAliveInterval, hasValidDateCustomFields],
    isUpdate: [isGroupEncrypted, hasValidAliveInterval, hasValidDateCustomFields]
  }
}

personGroupSpec.getTransformation = GetTransformation(personGroupSpec)
// needed to prune legacy attributes, e.g. location, phone, email, address

module.exports = personGroupSpec
