// a personal profile for you alone, your "source of truth"
const SimpleSet = require('@tangle/simple-set')

const { string, gender, number, boolean, image, stringArray } = require('../lib/field-types')
const { isGroupEncrypted, hasValidAliveInterval } = require('../lib/validators')
const GetTransformation = require('../lib/get-transformation')

const personSourceSpec = {
  type: 'profile/person/source',
  props: {
    preferredName: string,
    legalName: string,
    altNames: SimpleSet(),

    avatarImage: image,
    headerImage: image,

    gender,
    description: string,

    aliveInterval: string,
    deceased: boolean,
    placeOfBirth: string,
    placeOfDeath: string,
    buriedLocation: string,
    birthOrder: number,

    city: string,
    country: string,
    postCode: string,

    profession: string,
    education: stringArray,
    school: stringArray,

    /* non-group */
    phone: string,
    email: string,
    address: string
  },
  hooks: {
    isRoot: [isGroupEncrypted, hasValidAliveInterval],
    isUpdate: [isGroupEncrypted, hasValidAliveInterval]
  }
}
personSourceSpec.getTransformation = GetTransformation(personSourceSpec)
// needed to prune legacy attributes, e.g. location

module.exports = personSourceSpec
