const API = require('./method')

const crut = {
  create: 'async',
  get: 'async',
  update: 'async',
  tombstone: 'async',
  list: 'async'
}

module.exports = {
  name: 'profile',
  version: require('./package.json').version,
  manifest: {
    person: {
      source: crut,
      group: {
        ...crut,
        findAdminProfileLinks: 'async'
      },
      admin: crut,
      public: crut
    },
    community: {
      group: crut,
      public: crut
    },
    pataka: {
      public: crut
    },

    find: 'async',

    link: {
      create: 'async'
    },
    findByFeedId: 'async',
    findByGroupId: 'async',
    findFeedsByProfileId: 'async',

    /* LEGACY - deprecated */
    publicPerson: crut,
    privatePerson: crut,

    publicCommunity: crut,
    privateCommunity: crut,

    publicPataka: crut,

    findFeedsByProfile: 'async'
  },
  init: (ssb) => {
    return API(ssb)
  }
}
