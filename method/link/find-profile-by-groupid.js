const { isCloakedMsg: isGroup } = require('ssb-ref')
const pull = require('pull-stream')
const paraMap = require('pull-paramap')
const pileSort = require('pile-sort')

module.exports = function FindByGroupId (ssb, crut, getProfile) {
  const getActiveProfile = (profileId, cb) => {
    getProfile(profileId, (err, profile) => {
      if (err) return cb(null, null)
      if (profile.states.every(state => state.tombstone)) return cb(null, null)
      // NOTE "null" profile means no profile / tombstoned

      cb(null, profile)
    })
  }

  return function findByGroupId (groupId, opts = {}, cb) {
    if (typeof opts === 'function') return findByGroupId(groupId, {}, opts)
    if (!isGroup(groupId)) return cb(new Error('requires a valid groupId'))

    // TODO replace with crut.list
    const query = [{
      $filter: {
        dest: groupId,
        value: {
          content: {
            type: 'link/group-profile',
            parent: groupId,
            tangles: {
              link: { root: null, previous: null }
            }
          }
        }
      }
    }]

    pull(
      ssb.backlinks.read({ query }),
      pull.filter(crut.isRoot),
      pull.map(link => link.value.content.child),
      paraMap(opts.getProfile || getActiveProfile, 4),
      pull.filter(Boolean), // drop null profiles
      pull.collect((err, results) => {
        if (err) return cb(err)

        const piles = pileSort(results, [
          res => !res.recps || res.recps.length === 0 // public
          // remainder are private
        ])

        cb(null, { public: piles[0], private: piles[1] })
      })
    )
  }
}
