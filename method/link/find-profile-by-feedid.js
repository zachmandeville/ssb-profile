const { isFeed } = require('ssb-ref')
const pull = require('pull-stream')
const paraMap = require('pull-paramap')
const pileSort = require('pile-sort')

module.exports = function FindByFeedId (ssb, crut, _getProfile) {
  function getActiveProfile (profileId, cb) {
    _getProfile(profileId, (err, profile) => {
      if (err) return cb(null, null) // don't rock the boat by erroring on a dud profile
      if (profile.states.every(state => state.tombstone)) return cb(null, null)
      // NOTE "null" profile means no profile / tombstoned

      cb(null, profile)
    })
  }

  return function findByFeedId (feedId, opts, cb) {
    if (typeof opts === 'function') return findByFeedId(feedId, {}, opts)
    if (!isFeed(feedId)) return cb(new Error('requires a valid feedId'))

    const {
      selfLinkOnly = true,
      groupId,
      sortPublicPrivate = true,
      getProfile = getActiveProfile
    } = opts

    // TODO replace with crut.list
    const query = [{
      $filter: {
        dest: feedId,
        value: {
          content: {
            type: 'link/feed-profile',
            parent: feedId,
            // child: profileId,
            tangles: {
              link: { root: null, previous: null }
            }
          }
        }
      }
    }]

    pull(
      ssb.backlinks.read({ query }),

      /* filters */
      selfLinkOnly
        ? pull.filter(m => m.value.author === feedId)
        : null,
      groupId
        ? pull.filter(m => m.value.content.recps && m.value.content.recps[0] === groupId)
        : null,
      pull.filter(crut.isRoot), // is root message for a link record

      /* load profile records */
      paraMap(
        (linkRoot, cb) => {
          getProfile(linkRoot.value.content.child, (err, profile) => {
            if (err) return cb(err)

            if (selfLinkOnly) cb(null, profile)
            else cb(null, { profile, linkAuthor: linkRoot.value.author })
          })
        },
        4
      ),
      pull.filter(Boolean), // prunes "duds"

      pull.collect((err, results) => {
        if (err) return cb(err)

        if (selfLinkOnly) {
          if (sortPublicPrivate) cb(null, sort.publicPrivate(results))
          else cb(null, results)
        } // eslint-disable-line
        else {
          const { self, other } = sort.selfOther(results, ssb.id)
          if (sortPublicPrivate) cb(null, { self: sort.publicPrivate(self), other: sort.publicPrivate(other) })
          else cb(null, { self, other })
        }
      })
    )
  }
}

const sort = {
  selfOther (results, myFeedId) {
    const piles = pileSort(results, [
      ({ profile, linkAuthor }) => linkAuthor === myFeedId // self linked
      // remainder "other"
    ])

    return {
      self: piles[0].map(result => result.profile),
      other: piles[1].map(result => result.profile)
    }
  },
  publicPrivate (profiles) {
    const piles = pileSort(profiles, [
      profile => {
        return !profile.recps || profile.recps.length === 0 // public
      }
      // remainder are private
    ])

    return { public: piles[0], private: piles[1] }
  }
}
