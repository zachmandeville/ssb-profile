const { isMsgId } = require('ssb-ref')
const pull = require('pull-stream')
const paraMap = require('pull-paramap')
const pileSort = require('pile-sort')

module.exports = function FindAdminProfileLinks (ssb, crut) {
  return function findAdminProfileLinks (profileId, opts = {}, cb) {
    if (typeof opts === 'function') return findAdminProfileLinks(profileId, {}, opts)
    if (!isMsgId(profileId)) return cb(new Error('requires a valid profileId'))

    // TODO replace with crut.list
    const query = [{
      $filter: {
        dest: profileId,
        value: {
          content: {
            type: 'link/profile-profile/admin',
            // parent: profileId
            // OR
            // child: profileId
            tangles: {
              link: { root: null, previous: null }
            }
          }
        }
      }
    }]

    pull(
      ssb.backlinks.read({ query }),
      pull.filter(m => (
        m.value.content.parent === profileId ||
        m.value.content.child === profileId
        // just quickly check to see if this backlink is a related with
        // our target profileId (as parent or child)
      )),
      pull.filter(crut.isRoot),
      pull.filter(Boolean), // drop null profiles
      paraMap((rawLink, cb) => {
        crut.read(rawLink.key, cb)
      }, 4),
      pull.filter(link => link.tombstone === null),
      pull.collect((err, results) => {
        if (err) return cb(err)

        const piles = pileSort(results, [
          link => link.child === profileId
        ])

        cb(null, { parentLinks: piles[0], childLinks: piles[1] })
      })
    )
  }
}
