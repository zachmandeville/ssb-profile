const CrutAuthors = require('ssb-crut-authors')
const Crut = require('ssb-crut')
const Force = require('ssb-crut/lib/force')

const spec = require('../spec')
const fixLinkCreateArgs = require('../lib/fix-link-create-args')

const Find = require('./find') // byName
const FindByFeedId = require('./link/find-profile-by-feedid')
const FindByGroupId = require('./link/find-profile-by-groupid')
const FindFeedsByProfileId = require('./link/find-feeds-by-profileid')
const FindAdminProfileLinks = require('./link/find-admin-profile-links')

module.exports = function Method (ssb) {
  const crut = {}

  for (const superType in spec) {
    crut[superType] = {}
    for (const subType in spec[superType]) {
      crut[superType][subType] = crutify(ssb, spec[superType][subType])
    }
  }

  // NOTE these don't use authorship right now - i.e. anyone can update!
  // NOTE these may need forceUpdate if we ever expose update/tombstone APIs for these
  crut.link = {
    feed: new Crut(ssb, spec.link.feed),
    group: new Crut(ssb, spec.link.group),
    admin: new Crut(ssb, spec.link.admin)
  }

  crut.person.group.findAdminProfileLinks = FindAdminProfileLinks(ssb, crut.link.admin)

  const getProfile = (profileId, cb) => {
    ssb.get({ id: profileId, private: true }, (err, value) => {
      if (err) return cb(err)

      const { type, recps } = value.content
      let c // the specific crut for superType/subType

      if (type === 'profile/person') c = recps ? crut.person.group : crut.person.public
      if (type === 'profile/person/admin') c = crut.person.admin
      if (type === 'profile/person/source') c = crut.person.source
      if (type === 'profile/community') c = recps ? crut.community.group : crut.community.public
      if (type === 'profile/pataka' && !recps) c = crut.pataka.public
      if (!crut) return cb(new Error(`Unknown type ${type}`))

      c.get(profileId, cb)
    })
  }

  const getCommunity = (profileId, cb) => {
    ssb.get({ id: profileId, private: true }, (err, value) => {
      if (err) return cb(err)

      const subType = value.content.recps ? 'group' : 'public'
      crut.community[subType].get(profileId, cb)
    })
  }
  const findFeedsByProfileId = FindFeedsByProfileId(ssb, crut.link.feed)

  return {
    ...crut,

    find: Find(ssb, getProfile),

    link: {
      create (...args) {
        const { childId, opts, cb } = fixLinkCreateArgs(...args)

        ssb.get({ id: childId, private: true }, (err, profileRoot) => {
          if (err) return cb(err)

          if (!profileRoot.content.type.startsWith('profile/')) {
            return cb(new Error(`Expected ${childId} to be of type profile, got ${profileRoot.content.type}`))
            // TODO: Do better validation of profile
          }

          const { groupId, feedId, profileId, allowPublic, parentGroupId } = opts

          let recps
          if (profileRoot.content.recps) recps = profileRoot.content.recps

          const buildDetails = (parent) => ({ parent, child: childId, allowPublic, recps })

          // child is set, now we detect the parent.
          // the parent type helps define what sort of link this should be
          if (groupId) {
            crut.link.group.create({ ...buildDetails(groupId), parentGroupId }, buildGet(crut.link.group, cb))
          } else if (feedId) {
            crut.link.feed.create(buildDetails(feedId), buildGet(crut.link.feed, cb))
          } else if (profileId) {
            crut.link.admin.create(buildDetails(profileId), buildGet(crut.link.admin, cb))
          } else {
            // TODO: does this need to validate recps are in your personal group?
            crut.link.feed.create(buildDetails(ssb.id), buildGet(crut.link.feed, cb))
          }
        })
      }
    },
    findByFeedId: FindByFeedId(ssb, crut.link.feed, getProfile),
    findByGroupId: FindByGroupId(ssb, crut.link.group, getCommunity), // does this work?
    findFeedsByProfileId,

    /* LEGACY */
    publicPerson: crut.person.public,
    privatePerson: crut.person.group,

    privateCommunity: crut.community.group,
    publicCommunity: crut.community.public,

    publicPataka: crut.community.public,

    findFeedsByProfile: findFeedsByProfileId
  }
}

function crutify (ssb, spec) {
  const crut = new CrutAuthors(ssb, spec)
  const { forceUpdate, forceTombstone } = Force(crut)

  function get (profileId, cb) {
    // NOTE does not currently support promise signature
    return crut.read(profileId, (err, profile) => {
      if (err) return cb(err)

      const subtype = profile.type.replace('profile/', '')
      profile.states.forEach(state => {
        state.type = subtype
      })

      cb(null, profile)
    })
  }
  // - renames read -> get, and decorates state with subtype
  // - only exposes allowed methods
  return {
    ...crut,

    create: crut.create.bind(crut),
    read: get,
    update: forceUpdate,
    tombstone: forceTombstone,
    list: crut.list.bind(crut),

    get
  }
}

function buildGet (crut, cb) {
  return function (err, linkId) {
    if (err) return cb(err)
    crut.read(linkId, (err, linkRecord) => {
      if (err) return cb(err)

      cb(null, {
        ...linkRecord,
        // legacy: old links returned { value: { content: {...} } }
        value: {
          content: linkRecord
        }
      })
    })
  }
}
