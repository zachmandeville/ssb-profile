const FlumeViewSearch = require('flumeview-search')
const get = require('lodash.get')
const pull = require('pull-stream')
const paraMap = require('pull-paramap')

const isProfile = msg => {
  if (!msg.value.content.type) return false

  return msg.value.content.type.startsWith('profile')
  // this is a weak definition. We leave ssb-crut to do tighter validation
}

module.exports = function Find (ssb, getProfile) {
  const search = ssb._flumeUse(
    'profile-name-search',
    FlumeViewSearch(
      3, // version
      3, // min "word" length
      (msg) => { // function which sources searchable content
        if (typeof msg.value.content === 'string') return
        if (!msg.value.content.type.startsWith === 'profile/') {
          return
        }
        if (!isProfile(msg)) return

        return msgToNames(msg)
      }
    )
  )

  return function find (opts, cb) {
    const {
      name,
      type = 'person',
      groupId,
      includeTombstoned = false,
      decorateWithLinkedFeeds = false
    } = opts

    if (typeof name !== 'string' || name.length < 3) return cb(new Error('opts.name must be a String of length >= 3'))
    buildRecpsFilter(groupId, (err, recpsFilter) => {
      if (err) return cb(err)

      pull(
        search.query({ query: normalize(name) }),

        // ? filter by recps[0]
        recpsFilter ? pull.filter(recpsFilter) : null,

        pull.map(getProfileId),
        pull.unique(),

        // load the profile record
        paraMap(
          (profileId, cb) => {
            getProfile(profileId, (err, profile) => cb(null, err ? null : profile)) // swallow errors
          },
          5 // width
        ),
        pull.filter(Boolean), // filter out errors (null)

        // ? filter by type
        (typeof type === 'string') ? pull.filter(profile => profile.states[0].type === type) : null,

        // ? filter tombstoned
        (!includeTombstoned) ? pull.filter(profile => profile.states[0].tombstone === null) : null,

        // filter by name
        pull.filter(buildNameFilter(name)),

        // ? add profile.linkedFeeds
        decorateWithLinkedFeeds ? paraMap(addLinkedFeeds, 5) : null,

        pull.collect(cb)
      )
    })
  }

  function buildRecpsFilter (groupId, cb) {
    if (!groupId) return cb(null, null)

    const isGroupEncrypted = m => m.value.content.recps?.[0] === groupId
    if (!groupId.endsWith('cloaked')) return cb(null, isGroupEncrypted)

    ssb.tribes.poBox.get(groupId, (err, poBox) => {
      if (err) return cb(null, isGroupEncrypted)

      const isPOBoxEncrypted = m => m.value.content.recps?.[0] === poBox.poBoxId
      return cb(null, m => isGroupEncrypted(m) || isPOBoxEncrypted(m))
    })
  }

  function buildNameFilter (name) {
    const regexp = new RegExp(normalize(name), 'i')

    const isMatch = (personName) => {
      if (!personName) return false
      return normalize(personName).match(regexp)
    }

    return function nameFilter (profile) {
      const { preferredName, legalName, altNames } = profile.states[0]
      // TODO change to this when upgrade ssb-crut:
      // const { preferredName, legalName, altNames } = profile

      if (isMatch(preferredName)) return true
      if (isMatch(legalName)) return true
      if (altNames && altNames.find(isMatch)) return true

      return false
    }
  }

  function addLinkedFeeds (profile, cb) {
    profile.linkedFeeds = []

    ssb.profile.findFeedsByProfile(profile.key, (err, feeds) => {
      if (err) return cb(err)

      profile.linkedFeeds = Array.from(new Set(feeds))

      cb(null, profile)
    })
  }
}

function msgToNames (msg) {
  let names = [
    get(msg, 'value.content.preferredName.set'),
    get(msg, 'value.content.legalName.set')
  ]
  const altNames = get(msg, 'value.content.altNames')
  if (altNames) {
    names = [...names, ...Object.keys(altNames)]
  }

  return names
    .filter(Boolean)
    .map(name => normalize(name))
    .join(' ')
}

function getProfileId (msg) {
  return msg.value.content.tangles.profile.root || msg.key
}

// source: https://stackoverflow.com/questions/990904/remove-accents-diacritics-in-a-string-in-javascript
function normalize (name) {
  return name
    .normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '')
    .toLowerCase()
}
