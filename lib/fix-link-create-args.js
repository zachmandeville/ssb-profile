module.exports = function fixLinkCreateArgs (profileId, opts, cb) {
  if (typeof opts === 'function') return fixLinkCreateArgs(profileId, {}, opts)

  /* LEGACY */
  // function ({ type = 'feed-profile', profile, profileId, group, groupId, allowPublic }, cb) {
  if (typeof profileId === 'object') {
    // if (!warned) {
    //   warned = true
    //   console.warn('deprecated function signature used for ssb.profile.link.create, please update your code')
    // }

    let {
      profile, profileId: _profileId,
      group, groupId,
      allowPublic,
      parentGroupId
    } = profileId

    profileId = _profileId || profile
    groupId = groupId || group

    // opts
    allowPublic = allowPublic || opts.allowPublic
    parentGroupId = parentGroupId || opts.parentGroupId

    return fixLinkCreateArgs(profileId, { groupId, allowPublic, parentGroupId }, cb)
  }

  /* LEGACY */
  return { childId: profileId, opts, cb }
}
