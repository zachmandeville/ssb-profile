const test = require('tape')
const CRUT = require('ssb-crut-authors')

const Server = require('../../test-bot')
const privatePersonSpec = require('../../../spec').person.group
const publicPersonSpec = require('../../../spec').person.public

test('update (correctly constructed update message)', { objectPrintDepth: 15 }, t => {
  const server = Server({ tribes: true })
  const friendId = '@p59SUQGLSjWJja6OZfG664+QXkOrjeGsysdXLwonFZQ=.ed25519'
  const { isRoot } = new CRUT(server, privatePersonSpec)

  server.tribes.create({}, (_, { groupId, groupInitMsg } = {}) => {
    // Note that #create generally doesn't currently allow you to post 'remove'
    // actions in the initial update
    const initialA = {
      type: 'profile/person',
      preferredName: { set: 'mix' },
      altNames: {
        mixmix: -2,
        mixmixjellyfish: -1,
        'j a irving': 1
      },
      authors: {
        [server.id]: { 1: 1 } // mix was added at seq 1
      },
      aliveInterval: { set: '1984/2001' },
      birthOrder: { set: 1 },

      tangles: {
        profile: { root: null, previous: null }
      },
      recps: [groupId]
    }
    if (!isRoot(initialA)) throw new Error(isRoot.errorsString)

    const updateA = {
      preferredName: 'mixy',
      description: 'some fulla',
      aliveInterval: '1984/2001',
      birthOrder: 2,
      altNames: {
        add: ['mixmixjellyfish', 'jai'],
        remove: ['j a irving']
      },
      authors: {
        add: [friendId],
        remove: [server.id]
      }
      // recps // inheritted from root
    }

    server.publish(initialA, (err, root) => {
      if (err) throw err
      server.profile.person.group.update(root.key, updateA, (err, updateId) => {
        t.error(err, 'public an update usding person.group.update')

        server.get({ id: updateId, private: true }, (err, value) => {
          if (err) throw err

          const expectedA = {
            type: 'profile/person',
            preferredName: { set: 'mixy' },
            description: { set: 'some fulla' },
            altNames: {
              mixmixjellyfish: 2,
              jai: 1,
              'j a irving': -2
            },
            authors: {
              [server.id]: { 2: -1 },
              [friendId]: { 0: 1 }
            },
            aliveInterval: { set: '1984/2001' },
            birthOrder: { set: 2 },
            tangles: {
              group: { root: groupInitMsg.key, previous: [root.key] },
              profile: { root: root.key, previous: [root.key] }
            },
            recps: [groupId]
          }

          Object.keys(expectedA).forEach(key => {
            t.deepEqual(value.content[key], expectedA[key], key)
          })

          t.deepEqual(value.content, expectedA, 'well formed update')
          server.close()
          t.end()
        })
      })
    })
  })
})

test('update (private)', t => {
  const server = Server({ tribes: true })
  const friendId = '@p59SUQGLSjWJja6OZfG664+QXkOrjeGsysdXLwonFZQ=.ed25519'
  const { isRoot } = new CRUT(server, privatePersonSpec)

  // Note that #create generally doesn't currently allow you to post 'remove'
  // actions in the initial update

  server.tribes.create({}, (_, { groupId, groupInitMsg } = {}) => {
    const recps = [groupId]

    const initialA = {
      type: 'profile/person',
      preferredName: { set: 'mix' },
      altNames: {
        mixmix: -2,
        mixmixjellyfish: -1,
        'j a irving': 1
      },
      authors: {
        [server.id]: { 1: 1 }
      },
      aliveInterval: { set: '1984/2001' },
      birthOrder: { set: 1 },

      tangles: {
        profile: { root: null, previous: null }
      },
      recps
    }

    if (!isRoot(initialA)) throw new Error('not a profile')

    const updateA = {
      preferredName: 'mixy',
      description: 'some fulla',
      aliveInterval: '1984/2001',
      birthOrder: 2,
      altNames: {
        add: ['mixmixjellyfish', 'jai'],
        remove: ['j a irving']
      },
      authors: {
        add: [
          friendId
        ],
        remove: [
          server.id
        ]
      }
    }

    server.publish(initialA, (err, root) => {
      if (err) throw err

      server.profile.person.group.update(root.key, updateA, (err, updateId) => {
        t.equal(err, null, 'no error')
        server.get(updateId, (_, value) => {
          t.equal(typeof value.content, 'string', 'encrypted updates!')

          const expectedA = {
            type: 'profile/person',
            preferredName: { set: 'mixy' },
            description: { set: 'some fulla' },
            aliveInterval: { set: '1984/2001' },
            birthOrder: { set: 2 },
            altNames: {
              mixmixjellyfish: 2,
              jai: 1,
              'j a irving': -2
            },
            authors: {
              [friendId]: { 0: 1 },
              [server.id]: { 2: -1 }
            },
            tangles: {
              group: { root: groupInitMsg.key, previous: [root.key] },
              profile: { root: root.key, previous: [root.key] }
            },
            recps
          }

          server.get({ id: updateId, private: true, meta: true }, (_, msg) => {
            t.deepEqual(msg.value.content, expectedA, 'well formed update')
            server.close()
            t.end()
          })
        })
      })
    })
  })
})

// TODO perhaps you can't update a tombstoned record ... unless you undo the tombstone?
test('update (tombstone)', t => {
  const server = Server()
  const { isRoot } = new CRUT(server, publicPersonSpec)

  // Note that #create generally doesn't currently allow you to post 'remove'
  // actions in the initial update

  const initialA = {
    type: 'profile/person',
    preferredName: { set: 'mix' },
    authors: {
      [server.id]: { 1: 1 }
    },
    tangles: {
      profile: { root: null, previous: null }
    }
  }
  if (!isRoot(initialA)) throw new Error(isRoot.error)

  const date = Date.now()
  const updateA = {
    tombstone: { date }
  }

  server.publish(initialA, (err, root) => {
    if (err) throw err
    server.profile.person.public.update(root.key, updateA, (err, updateId) => {
      t.equal(err, null, 'no error')

      const expectedA = {
        type: 'profile/person',
        tombstone: { set: { date } },

        tangles: {
          profile: { root: root.key, previous: [root.key] }
        }
      }

      server.get({ id: updateId, private: true, meta: true }, (err, msg) => {
        if (err) throw err

        t.deepEqual(msg.value.content, expectedA, 'well formed update')
        server.close()
        t.end()
      })
    })
  })
})

test('update (empty altNames update)', t => {
  const server = Server({ tribes: true })
  server.tribes.create({}, (_, { groupId } = {}) => {
    const content = {
      preferredName: 'mix',
      authors: {
        add: [server.id]
      },
      recps: [groupId]
    }
    // make a profile
    server.profile.person.group.create(content, (err, profileId) => {
      if (err) throw err

      const update = {
        preferredName: 'mixmix',
        altNames: { add: [] }
      }
      server.profile.person.group.update(profileId, update, (err) => {
        t.error(err, 'allows updates which have empty altNames input')
        server.close(t.end)
      })
    })
  })
})

test('update (empty update)', t => {
  const server = Server()

  const content = {
    preferredName: 'mix',
    authors: {
      add: [server.id]
    }
  }
  // make a profile
  server.profile.person.public.create(content, (_, profileId) => {
    const update = {}
    server.profile.person.public.update(profileId, update, (err) => {
      t.error(err, 'empty updates are allowed right now (developers fault)')
      server.close(t.end)
    })
  })
})

test('update (clear a field)', t => {
  const server = Server({ tribes: true })

  server.tribes.create({}, (_, { groupId } = {}) => {
    const content = {
      preferredName: 'mix',
      deceased: false,
      authors: {
        add: [server.id]
      },
      recps: [groupId]
    }
    // make a profile
    server.profile.person.group.create(content, (_, profileId) => {
      const update = {
        preferredName: null,
        deceased: null
      }
      server.profile.person.group.update(profileId, update, (err) => {
        t.error(err, 'publish update')
        server.profile.person.group.get(profileId, (_, profile) => {
          t.equal(profile.states[0].preferredName, null, 'cleared field')
          t.equal(profile.states[0].deceased, null, 'cleared field')
          server.close(t.end)
        })
      })
    })
  })
})
