const test = require('tape')
const Server = require('../../test-bot')
const { isMsgId } = require('ssb-ref')
const { promisify: p } = require('util')

test('profile/person/group custom fields', async t => {
  // ////
  // Setup
  // ////
  const ssb = Server({ tribes: true })

  let i = 0
  const generateTimestamp = () => {
    return (Date.now() + (i += 10)).toString()
  }

  const keyA = generateTimestamp()
  const keyB = generateTimestamp()
  const keyC = generateTimestamp()
  const keyD = generateTimestamp()
  const keyE = generateTimestamp()
  const keyF = generateTimestamp()

  // create the group and admin subgroup
  const { groupId } = await p(ssb.tribes.create)({})
  // await p(ssb.tribes.subtribe.create)(groupId, { admin: true })

  const customFields = {
    [keyA]: 'Hamilton', // Hometown
    [keyB]: ['Bachelor of Science', 'NCEA'], // Qualifications
    [keyC]: ['Self-employed'], // Employment Status
    [keyD]: true, // Living in NZ
    [keyE]: 26, // age
    [keyF]: 1.23123 // shares
  }

  // create an owned profile in the parent group
  let details = {
    preferredName: 'Cherese',
    customFields,
    authors: { add: [ssb.id] },
    recps: [groupId]
  }

  const groupProfileId = await p(ssb.profile.person.group.create)(details)
  t.true(isMsgId(groupProfileId), 'initial person/group profile was made with customFields')

  let profile = await p(ssb.profile.person.group.get)(groupProfileId)
  t.deepEqual(
    profile.customFields,
    customFields,
    'gets the correct initial custom fields'
  )

  // update
  details = {
    customFields: {
      [keyA]: 'Hamilton, New Zealand', // update
      [keyB]: ['Bachelor of Science'], // remove one item
      [keyD]: null // delete
    }
  }

  await p(ssb.profile.person.group.update)(groupProfileId, details)
  profile = await p(ssb.profile.person.group.get)(groupProfileId)

  t.deepEqual(
    profile.customFields,
    {
      [keyA]: 'Hamilton, New Zealand', // Hometown
      [keyB]: ['Bachelor of Science'], // Qualifications
      [keyC]: ['Self-employed'], // Employment Status
      [keyD]: null, // Living in NZ
      [keyE]: 26, // age
      [keyF]: 1.23123 // shares
    },
    'gets the correct updated custom fields'
  )

  ssb.close()
  t.end()
})

test('profile/person/admin custom fields', async t => {
  // ////
  // Setup
  // ////
  const ssb = Server({ tribes: true })

  let i = 0
  const generateTimestamp = () => {
    return (Date.now() + (i += 10)).toString()
  }

  const keyA = generateTimestamp()
  const keyB = generateTimestamp()
  const keyC = generateTimestamp()
  const keyD = generateTimestamp()
  const keyE = generateTimestamp()
  const keyF = generateTimestamp()

  // create the group and admin subgroup
  const { groupId } = await p(ssb.tribes.create)({})
  const { poBoxId } = await p(ssb.tribes.subtribe.create)(groupId, { admin: true, addPOBox: true })

  const customFields = {
    [keyA]: 'Hamilton', // Hometown
    [keyB]: ['Bachelor of Science', 'NCEA'], // Qualifications
    [keyC]: ['Self-employed'], // Employment Status
    [keyD]: true, // Living in NZ
    [keyE]: 26, // age
    [keyF]: 1.23123 // shares
  }

  // create an owned profile in the parent group
  let details = {
    preferredName: 'Cherese',
    customFields,
    authors: { add: [ssb.id] },
    recps: [poBoxId]
  }

  const adminProfileId = await p(ssb.profile.person.admin.create)(details)
  t.true(isMsgId(adminProfileId), 'initial person/admin profile was made with customFields')

  let profile = await p(ssb.profile.person.admin.get)(adminProfileId)
  t.deepEqual(
    profile.customFields,
    customFields,
    'gets the correct initial custom fields'
  )

  // update
  details = {
    customFields: {
      [keyA]: 'Hamilton, New Zealand', // update
      [keyB]: ['Bachelor of Science'], // remove one item
      [keyD]: null // delete
    }
  }

  await p(ssb.profile.person.admin.update)(adminProfileId, details)
  profile = await p(ssb.profile.person.admin.get)(adminProfileId)

  t.deepEqual(
    profile.customFields,
    {
      [keyA]: 'Hamilton, New Zealand', // Hometown
      [keyB]: ['Bachelor of Science'], // Qualifications
      [keyC]: ['Self-employed'], // Employment Status
      [keyD]: null, // Living in NZ
      [keyE]: 26, // age
      [keyF]: 1.23123 // shares
    },
    'gets the correct updated custom fields'
  )

  ssb.close()
  t.end()
})
