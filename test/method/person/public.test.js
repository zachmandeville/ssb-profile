const test = require('tape')

const Server = require('../../test-bot')

// tests:
// - avatarImage fields (tricky shaped field)
//   - defer to privateProfile
//
// - legacy fields disallowed
// - recps-guard

test('ssb.profile.publicPerson.* (legacy fields disallowed)', t => {
  let plan = 3
  t.plan(3)

  const server = Server({ recpsGuard: true })

  const done = () => {
    if (--plan === 0) server.close()
  }

  // create
  const legacyInput = {
    preferredName: 'mix',
    phone: '026 1233123123', // WOOPS!
    authors: { add: [server.id] },
    allowPublic: true
  }
  server.profile.publicPerson.create(legacyInput, (err) => {
    t.match(err.message, /unallowed inputs: phone/, 'create blocks legacy fields ')

    done()
  })

  // update
  const modernInput = {
    preferredName: 'mixmix',
    authors: { add: [server.id] },
    allowPublic: true
  }
  server.profile.publicPerson.create(modernInput, (_, profileId) => {
    const legacyInput = {
      phone: '026 1233123123', // WOOPS!
      allowPublic: true
    }

    server.profile.publicPerson.update(profileId, legacyInput, (err) => {
      t.match(err.message, /unallowed update input: "?phone"?/, 'update blocks legacy fields')

      done()
    })
  })

  // get
  const content = {
    type: 'profile/person',
    preferredName: { set: 'Alice Private' },
    phone: { set: '2833312312312' },
    authors: {
      [server.id]: { 1: 1 }
    },
    tangles: {
      profile: { root: null, previous: null }
    }
  }
  server.publish({ content, options: { allowPublic: true } }, (err, msg) => {
    if (err) throw err
    const profileId = msg.key

    const content = {
      type: 'profile/person',
      phone: { set: '111111111' },
      tangles: {
        profile: { root: profileId, previous: [profileId] }
      }
    }

    server.publish({ content, options: { allowPublic: true } }, (err, msg) => {
      if (err) throw err
      const updateId = msg.key

      server.profile.publicPerson.get(profileId, (_, profile) => {
        const state = {
          tombstone: null,
          preferredName: 'Alice Private',
          gender: null,
          avatarImage: null,
          authors: {
            [server.id]: [{ start: 1, end: null }]
          }
        }
        t.deepEqual(
          profile,
          {
            key: profileId,
            type: 'profile/person',
            originalAuthor: server.id,
            recps: null,
            states: [{
              key: updateId,
              type: 'person',
              ...state
            }],
            ...state,
            conflictFields: []
          },
          'get ignores any legacy fields of existing messages'
        )

        done()
      })
    })
  })
})

test('ssb.profile.publicPerson.* (ssb-recps-guard)', t => {
  const server = Server({ recpsGuard: true })

  const input = {
    preferredName: 'mix',
    authors: { add: [server.id] },
    allowPublic: true
  }
  server.profile.publicPerson.create(input, (err, profileId) => {
    t.error(err, 'allowed to create publicPerson with allowPublic')

    const input = {
      preferredName: 'mix',
      authors: { add: [server.id] }
      // allowPublic: true
    }

    server.profile.publicPerson.create(input, (err) => {
      t.match(err.message, /recps-guard/, 'blocked without allowPublic')

      const input = {
        preferredName: 'mixmix',
        authors: { add: ['*'], remove: [server.id] },
        allowPublic: true
      }
      server.profile.publicPerson.update(profileId, input, (err) => {
        t.error(err, 'allowed to update publicPerson with allowPublic')

        const input = {
          preferredName: 'mixmixmix',
          authors: { add: ['*'], remove: [server.id] }
          // allowPublic: true
        }
        server.profile.publicPerson.update(profileId, input, (err) => {
          t.match(err.message, /recps-guard/, 'blocked without allowPublic')
          server.close()
          t.end()
        })
      })
    })
  })
})
