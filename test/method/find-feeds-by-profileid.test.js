const test = require('tape')
const { promisify: p } = require('util')
const { replicate } = require('scuttle-testbot')

const Server = require('../test-bot')

test('findFeedsByProfileId', async t => {
  const alice = Server({ tribes: true })
  const bob = Server({ tribes: true })

  // const { groupId } = await p(alice.tribes.create)({})

  const profileA = await p(alice.profile.person.public.create)({
    authors: { add: [alice.id] }
  })
  await p(alice.profile.link.create)(profileA)
  const profileB = await p(alice.profile.person.public.create)({
    authors: { add: [alice.id] }
  })
  await replicate({ from: alice, to: bob })

  await p(bob.profile.link.create)(profileB, { feedId: alice.id })
  await replicate({ from: bob, to: alice })

  let feeds
  feeds = await p(alice.profile.findFeedsByProfileId)(profileA)
  t.deepEqual(feeds, [alice.id], 'classic')

  feeds = await p(alice.profile.findFeedsByProfileId)(profileA, { selfLinkOnly: false })
  t.deepEqual(
    feeds,
    {
      self: [alice.id],
      other: []
    },
    'selfLinkOnly: false'
  )
  feeds = await p(alice.profile.findFeedsByProfileId)(profileB, { selfLinkOnly: false })
  t.deepEqual(
    feeds,
    {
      self: [],
      other: [alice.id]
    },
    'selfLinkOnly: false'
  )

  alice.close()
  bob.close()

  t.end()
})
