const test = require('tape')
const pull = require('pull-stream')
const Server = require('../test-bot')

function setup (profileDetails, next) {
  const server = Server({ tribes: true })
  server.tribes.create({}, (_, { groupId }) => {
    pull(
      pull.values(profileDetails),
      pull.asyncMap(createProfileAndLink),
      pull.collect((err, links) => {
        if (err) throw err
        next({ server, links, groupId })
      })
    )

    function createProfileAndLink ({ name, country, encrypt }, cb) {
      const details = {
        preferredName: name,
        country,
        authors: { add: [server.id] }
      }
      if (!country) delete details.country
      if (encrypt) {
        details.recps = [groupId]
        details.allowWhakapapaViews = true
        details.allowPersonsList = false
        details.allowStories = null
      }

      const subType = encrypt ? 'group' : 'public'

      server.profile.community[subType].create(details, (err, profileId) => {
        if (err) return cb(err)

        server.profile.link.create(profileId, { groupId }, (err, link) => {
          if (err) return cb(err)
          server.get({ id: link.key, private: true, meta: true }, cb)
        })
      })
    }
  })
}

test('findByGroupId', t => {
  t.plan(10)

  /* finds all profiles */
  const profileDetails = [
    { name: 'Te Āti Awa' },
    { name: 'Wellington Iwi' },
    { name: 'Te Āti Awa', country: 'New Zealand', encrypt: true }
  ]

  setup(profileDetails, ({ server, links, groupId }) => {
    server.profile.findByGroupId(groupId, (err, profiles) => {
      if (err) throw err
      const profileIds = links
        .map(link => link.value.content.child)

      t.equal(profiles.public.length, 2, 'two public communities')
      t.deepEqual(profiles.public.map(p => p.key), profileIds.slice(0, 2), 'returns keys of users public profiles')
      t.equal(profiles.private.length, 1, 'one private community')
      t.deepEqual(profiles.private.map(p => p.key), profileIds.slice(2), 'returns keys of users private profiles')

      // NOTE oldest first!
      const publicState = [
        fullState({
          key: profiles.public[0].states[0].key, // cheat these keys in because too hard
          type: 'community',
          preferredName: 'Te Āti Awa',
          authors: { [server.id]: [{ start: 1, end: null }] }
        }),

        fullState({
          key: profiles.public[1].states[0].key,
          type: 'community',
          preferredName: 'Wellington Iwi',
          authors: { [server.id]: [{ start: 3, end: null }] }
        })
      ]

      const privateState = [
        fullState({
          key: profiles.private[0].states[0].key,
          type: 'community',
          preferredName: 'Te Āti Awa',
          country: 'New Zealand',
          allowWhakapapaViews: true,
          allowPersonsList: false,
          allowStories: null,
          authors: { [server.id]: [{ start: 5, end: null }] }
        }, { private: true })
      ]

      t.deepEqual(profiles.public.map(p => p.states[0]), publicState, 'returns state of public profiles')
      t.deepEqual(profiles.private.map(p => p.states[0]), privateState, 'returns state of private profiles')

      server.close()
    })
  })

  // finds no profile
  const server = Server({ tribes: true })
  server.tribes.create({}, (_, { groupId }) => {
    server.profile.findByGroupId(groupId, (_, profiles) => {
      t.deepEqual(profiles.public, [], 'returns empty array of public profiles')
      t.deepEqual(profiles.private, [], 'returns empty array of private profiles')

      server.close()
    })
  })

  // finds no tombstoned profiles
  setup([{ name: 'Max' }], ({ server, links, groupId }) => {
    const profileId = links[0].value.content.child
    const update = {
      tombstone: { date: Date.now(), reason: 'typo' }
    }
    server.profile.publicCommunity.update(profileId, update, (err) => {
      if (err) throw err
      server.profile.findByGroupId(groupId, (err, profiles) => {
        if (err) throw err
        t.deepEqual(profiles.public, [], 'does not find tombstoned records!')

        server.close()
      })
    })
  })

  // don't pass in a groupId!
  const server2 = Server()
  server2.profile.findByGroupId('cat', (err) => {
    t.match(err.message, /requires a valid groupId/)

    server2.close()
  })

  // TODO test looking up a valid groupId which you know nothing about
})

function fullState (someState, opts = {}) {
  const base = {
    type: '???',
    preferredName: null,
    authors: {},

    description: null,

    avatarImage: null,
    headerImage: null,

    address: null,
    city: null,
    country: null,
    postCode: null,

    phone: null,
    email: null,

    joiningQuestions: [],
    customFields: {},

    tombstone: null,
    poBoxId: null
  }

  if (opts.private) {
    delete base.joiningQuestions
    delete base.customFields
  }

  return Object.assign(base, someState)
}
