const test = require('tape')
const pull = require('pull-stream')
const pick = require('lodash.pick')
const { promisify: p } = require('util')

const Server = require('../test-bot')

function setup (profileDetails, next) {
  const server = Server({ tribes: true })

  server.tribes.create({}, (err, { groupId } = {}) => {
    if (err) throw err

    pull(
      pull.values(profileDetails),
      pull.asyncMap(({ type, details, update, link }, cb) => {
        details.authors = { add: [server.id] }
        if (details.recps) details.recps = [groupId]
        const [superType, subType] = getTypePath(type, details.recps)

        const crut = server.profile[superType][subType]

        crut.create(details, (err, profileId) => {
          if (err) return cb(err)

          if (!update) {
            if (!link) return cb(null, profileId)
            server.profile.link.create(profileId, (err, link) => {
              if (err) return cb(err)

              cb(null, profileId)
            })
          } else crut.update(profileId, update, cb)
        })
      }),
      pull.collect((err, profiles) => {
        if (err) throw err
        next(server, groupId)
      })
    )
  })

  function getTypePath (typeStr, recps) {
    const typePath = typeStr.split('/')
    if (typePath.length === 1) typePath.push(recps ? 'group' : 'public')

    return typePath
  }
}

test('find', t => {
  t.plan(9)

  /* search "ben" and get all the people with "ben*" in in any name */
  setup(
    [
      {
        type: 'person',
        details: { preferredName: 'Ben1' }
      },
      {
        type: 'person',
        details: { preferredName: 'ben2' }
      },
      {
        type: 'person',
        details: { legalName: 'dave ben', recps: true }
      },
      {
        type: 'person',
        details: { altNames: { add: ['benjamin'] }, recps: true }
      },
      {
        type: 'person',
        details: { preferredName: 'mr bean' }
      }
    ],
    (server) => {
      server.profile.find({ type: 'person', name: 'ben' }, (err, profiles) => {
        t.error(err, 'runs find')

        profiles = profiles.map(p => p.states[0])

        t.true(
          profiles.every(p => (
            (p.preferredName && p.preferredName.match(/ben/i)) ||
            (p.legalName && p.legalName.match(/ben/i)) ||
            (p.altNames && p.altNames.find(name => name.match(/ben/i)))
          )),
          'gets right names'
        )
        // console.log(profiles.map(p => pick(p, ['preferredName', 'altNames'])))

        server.close()
      })
    }
  )

  /* scoped to correct profile type */
  setup(
    [
      {
        type: 'person',
        details: { preferredName: 'aurora (person)' }
      },
      {
        type: 'community',
        details: { preferredName: 'aurora (community)' }
      }
    ],
    (server) => {
      server.profile.find({ type: 'community', name: 'aur' }, (err, profiles) => {
        if (err) throw err
        profiles = profiles.map(p => p.states[0])

        t.deepEqual(
          profiles.map(p => pick(p, ['type', 'preferredName'])),
          [{ type: 'community', preferredName: 'aurora (community)' }],
          'type: community - searches the right types'
        )

        server.close()
      })
    }
  )

  // type: null
  setup(
    [
      {
        type: 'person',
        details: {
          preferredName: 'aurora (person)',
          recps: true
        }
      },
      {
        type: 'person/source',
        details: {
          preferredName: 'aurora (source)',
          recps: true
        }
      }
    ],
    (server, groupId) => {
      server.profile.find({ type: null, name: 'aur' }, (err, profiles) => {
        if (err) throw err
        profiles = profiles
          .map(profile => ({
            ...pick(profile.states[0], ['type', 'preferredName', 'recps']),
            recps: profile.recps
          }))
          .reverse()

        t.deepEqual(
          profiles,
          [
            { type: 'person', preferredName: 'aurora (person)', recps: [groupId] },
            { type: 'person/source', preferredName: 'aurora (source)', recps: [groupId] }
          ],
          'type: null - searches the right types'
        )

        server.close()
      })
    }
  )

  /* scope by groupId */
  setup(
    [
      {
        type: 'person',
        details: {
          preferredName: 'aurora (public)'
        }
      },
      {
        type: 'person',
        details: {
          preferredName: 'aurora (group)',
          recps: true
        }
      }
    ],
    (server, groupId) => {
      server.profile.find({ type: 'person', groupId, name: 'aur' }, (err, profiles) => {
        if (err) throw err
        profiles = profiles
          .map(profile => ({
            ...pick(profile.states[0], ['type', 'preferredName', 'recps']),
            recps: profile.recps
          }))

        t.deepEqual(
          profiles,
          [
            { type: 'person', preferredName: 'aurora (group)', recps: [groupId] }
          ],
          'groupId - returns only methods profiles in right group'
        )

        server.close()
      })
    }
  )

  /* tombstone records */
  setup(
    [
      {
        type: 'person',
        details: { preferredName: 'big ben' },
        update: { tombstone: { date: Date.now() } }
      }
    ],
    (server) => {
      server.profile.find({ type: 'person', name: 'ben' }, (_, profiles) => {
        t.equal(profiles.length, 0, 'does not find tombstoned records')

        server.profile.find({ type: 'person', name: 'ben', includeTombstoned: true }, (_, profiles) => {
          t.equal(profiles.length, 1, 'includes tombstoned records (includeTombstoned: true)')

          server.close()
        })
      })
    }
  )

  /* updates records */
  setup(
    [
      {
        type: 'person',
        details: { preferredName: 'benjamin' },
        update: { preferredName: 'jim' }
      },
      {
        type: 'person',
        details: { preferredName: 'bj' },
        update: { preferredName: 'ben james' }
      }
    ],
    (server) => {
      server.profile.find({ type: 'person', name: 'Ben' }, (_, profiles) => {
        t.deepEqual(
          profiles.map(p => p.states[0].preferredName),
          ['ben james'],
          'takes updates into account'
        )

        server.close()
      })
    }
  )

  /* test link decoration */
  setup(
    [
      {
        type: 'person',
        details: { preferredName: 'benjamin' },
        link: true
      },
      {
        type: 'person',
        details: { preferredName: 'ben' },
        link: false
      }
    ],
    (server) => {
      server.profile.find({ type: 'person', name: 'Ben', decorateWithLinkedFeeds: true }, (_, profiles) => {
        t.deepEqual(
          profiles.map(p => p.linkedFeeds),
          [[], [server.id]],
          'decorate with linked feeds'
        )

        server.close()
      })
    }
  )
})

test('find (admin profiles)', async t => {
  const ssb = Server({ tribes: true })

  const { groupId, poBoxId } = await p(ssb.tribes.create)({ addPOBox: true })

  const profileId1 = await p(ssb.profile.person.admin.create)({
    preferredName: 'dave one',
    authors: { add: ['*'] },
    recps: [groupId]
  })

  const profileId2 = await p(ssb.profile.person.admin.create)({
    preferredName: 'dave two',
    authors: { add: ['*'] },
    recps: [poBoxId]
  })

  const profiles = await p(ssb.profile.find)({ name: 'dave', type: 'person/admin', groupId })

  t.deepEqual(
    profiles.map(p => p.key),
    [profileId2, profileId1],
    'finds profiles from groupId and associated profileId'
  )

  ssb.close()
  t.end()
})

test('find (with macron)', t => {
  t.plan(4)
  /* search "maui" and get all the people with "maui" in in any name, including with macrons and special characters */
  setup(
    [
      {
        type: 'person',
        details: { preferredName: 'Māui-pōtiki' } // with macrons
      },
      {
        type: 'person',
        details: { preferredName: 'Maui' } // without macrons
      }
    ],
    (server) => {
      server.profile.find({ type: 'person', name: 'maui' }, (err, profilesA) => {
        t.error(err, 'runs find on maui (no macron)')

        profilesA = profilesA.map(p => p.states[0].preferredName)

        t.deepEqual(
          profilesA,
          ['Maui', 'Māui-pōtiki'],
          'returns both maui profiles'
        )

        server.profile.find({ type: 'person', name: 'māui' }, (err, profilesB) => {
          t.error(err, 'runs find on māui (macron)')
          profilesB = profilesB.map(p => p.states[0].preferredName)

          t.deepEqual(profilesA, profilesB, 'returns same for search without macrons')
          server.close()
        })
      })
    }
  )
})
