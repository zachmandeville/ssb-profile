const test = require('tape')
const Server = require('../../test-bot')

test('profile/community custom fields', t => {
  const server = Server({ recpsGuard: true })
  let i = 0

  const generateTimestamp = () => {
    return (Date.now() + (i += 10)).toString()
  }

  const timestampA = generateTimestamp()
  const timestampB = generateTimestamp()
  const timestampC = generateTimestamp()
  const timestampD = generateTimestamp()
  const timestampE = generateTimestamp()
  const timestampF = generateTimestamp()
  const timestampG = generateTimestamp()

  const customFields = {
    [timestampA]: {
      type: 'text',
      label: 'Hometown',
      order: 1,
      required: false,
      visibleBy: 'members'
    },
    [timestampB]: {
      type: 'array',
      label: 'Qualifications',
      order: 2,
      required: false,
      visibleBy: 'members'
    },
    [timestampC]: {
      type: 'list',
      label: 'Employement Status',
      required: false,
      multiple: false,
      order: 3,
      options: ['Employed', 'Unemployed', 'Self-emplyed', 'Student'],
      visibleBy: 'members'
    },
    [timestampD]: {
      type: 'checkbox',
      label: 'Living in NZ',
      order: 4,
      required: false,
      visibleBy: 'admin'
    },
    [timestampE]: {
      type: 'number',
      label: 'How old are you?',
      order: 4,
      required: false,
      visibleBy: 'admin'
    },
    [timestampF]: {
      type: 'number',
      label: 'How many shares do you have in X land?',
      order: 4,
      required: false,
      visibleBy: 'admin'
    },
    [timestampG]: {
      type: 'date',
      label: 'Date suceeded',
      order: 5,
      required: false,
      visibleBy: 'members'
    }
  }

  const details = {
    preferredName: 'Farm Trust',
    customFields,
    authors: { add: [server.id] },
    allowPublic: true
  }

  // a kaitiaki creates a new community
  server.profile.community.public.create(details, (err, profileId) => {
    t.error(err, 'no error when creating a community profile')

    if (!profileId) throw new Error('failed to create')

    server.profile.community.public.get(profileId, (err, profile) => {
      t.error(err, 'get community profile without error')

      t.deepEqual(
        profile.customFields,
        customFields,
        'returns the custom fields that were defined'
      )

      t.deepEqual(Object.keys(profile.customFields), [timestampA, timestampB, timestampC, timestampD, timestampE, timestampF, timestampG], 'all keys are the timestamps')

      const updateDetails = {
        customFields: {
          [timestampC]: {
            type: 'list',
            label: 'Employement Status',
            required: true,
            multiple: false,
            order: 3,
            options: ['Employed', 'Unemployed', 'Self-Employed', 'Student'],
            visibleBy: 'members'
          }
        },
        allowPublic: true // this is needed for the recps guard
      }
      server.profile.community.public.update(profileId, updateDetails, (err) => {
        t.error(err, 'update community profile without error')

        server.profile.community.public.get(profileId, (err, profile) => {
          t.error(err, 'get updated community profile without errors')

          // overwrite the custom field we updated
          customFields[timestampC] = updateDetails.customFields[timestampC]

          t.deepEqual(
            profile.customFields,
            customFields,
            'returns the updated custom fields!'
          )

          const tombstoneDetails = {
            customFields: {
              [timestampF]: {
                type: 'number',
                label: 'How many shares do you have in X land?',
                order: 4,
                required: false,
                visibleBy: 'admin',
                tombstone: {
                  date: Date.now(),
                  reason: 'user deleted field'
                }
              }
            },
            allowPublic: true
          }

          server.profile.community.public.update(profileId, tombstoneDetails, (err) => {
            t.error(err, 'tombstone custom field without error')

            server.profile.community.public.get(profileId, (err, profile) => {
              t.error(err, 'gets tombstoned custom field without error')

              t.deepEqual(
                profile.customFields[timestampF],
                tombstoneDetails.customFields[timestampF],
                'tombstone field was saved on the custom field'
              )
              server.close(t.end)
            })
          })
        })
      })
    })
  })
})
