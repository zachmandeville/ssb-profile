const test = require('tape')
const CRUT = require('ssb-crut-authors')
const publicCommunitySpec = require('../../../spec').community.public
const Server = require('../../test-bot')

test('profile/community/public customFields', t => {
  t.plan(22)
  const mock = (value) => {
    return {
      type: 'profile/community',
      customFields: value,
      tangles: {
        profile: { root: null, previous: null }
      }
    }
  }

  let i = 0
  const generateTimestamp = () => {
    return (Date.now() + (i += 10)).toString()
  }

  const server = Server()
  const { isRoot } = new CRUT(server, publicCommunitySpec)

  const mockTest = (value, isError = false) => {
    const timestamp = generateTimestamp()
    const field = {
      [timestamp]: value
    }

    if (isError) {
      t.false(isRoot(mock(field)), `isRoot(${value}) => false`)
      t.notEquals(isRoot.errors, null, `customFields: ${value} => returns errors`)
      return
    }

    t.true(isRoot(mock(field)), `isRoot ${value.type}`)
    t.equals(isRoot.errors, null, `customFields: ${value.type} => no errors`)
  }

  const mockFailingTest = (value) => mockTest(value, true)

  // text: a string as the value
  mockTest({
    type: 'text',
    label: 'Hometown',
    order: 1,
    required: false,
    visibleBy: 'members'
  })

  // multiple response: the value is an array of strings
  mockTest({
    type: 'array',
    label: 'Qualifications',
    order: 2,
    required: false,
    visibleBy: 'members'
  })

  // select: choose from a list of options
  mockTest({
    type: 'list',
    label: 'Employement Status',
    required: false,
    order: 3,
    multiple: true,
    options: ['Employed', 'Unemployed', 'Self-emplyed', 'Student'],
    visibleBy: 'admin'
  })

  // checkbox: true/false value
  mockTest({
    type: 'checkbox',
    label: 'Living in NZ',
    order: 4,
    required: false,
    visibleBy: 'admin'
  })

  // date: a date string as the value
  mockTest({
    type: 'date',
    label: 'date succeeded',
    order: 5,
    required: false,
    visibleBy: 'members'
  })

  // failing test for empty customFields
  mockFailingTest(null)
  mockFailingTest({})

  // failing test for missing required field
  mockFailingTest({
    type: 'checkbox',
    label: 'Living in NZ',
    order: 4,
    required: false
  })

  // failing test for additional field
  mockFailingTest({
    type: 'checkbox',
    label: 'Living in NZ',
    order: 4,
    required: false,
    um: 'Not supposed to be here'
  })

  // failing test for missing options on list
  mockFailingTest({
    type: 'list',
    label: 'Employement Status',
    multiple: true,
    required: false,
    order: 3,
    visibleBy: 'admin'
  })

  // failing test for missing multiple
  mockFailingTest({
    type: 'list',
    label: 'Employement Status',
    required: false,
    order: 3,
    visibleBy: 'admin',
    options: ['Employed', 'Unemployed', 'Self-emplyed', 'Student']
  })

  server.close()
})
