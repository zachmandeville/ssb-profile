const test = require('tape')
const CRUT = require('ssb-crut-authors')
const publicCommunitySpec = require('../../../spec').community.public
const Server = require('../../test-bot')

test('publicCommunitySpec joiningQuestion isRoot', t => {
  t.plan(18)
  const mock = (value) => {
    return {
      type: 'profile/community',
      joiningQuestions: {
        set: value
      },
      tangles: {
        profile: { root: null, previous: null }
      }
    }
  }

  const server = Server()
  const { isRoot } = new CRUT(server, publicCommunitySpec)

  // undefined
  t.false(isRoot(mock()), 'undefined')
  t.true(isRoot.errors.length > 0, 'undefined => errors')

  //  null joining questions
  t.true(isRoot(mock(null)), 'null')
  t.equal(isRoot.errors, null, 'joiningQuestions: null => no errors')

  // empty array joining questions
  t.true(isRoot(mock([])), '[]')
  t.equal(isRoot.errors, null, 'joiningQuestions: [] => no errors')

  t.true(isRoot(mock([{
    type: 'input',
    label: 'Ko wai koe?'
  }])), 'single input')
  t.equal(isRoot.errors, null, 'joiningQuestions: [] => no errors')

  t.true(isRoot(mock([
    { type: 'input', label: 'Ko wai koe?' },
    { type: 'textarea', label: 'Ko wai to ingoa?' }
  ])), 'multiple input')
  t.equal(isRoot.errors, null, 'joiningQuestions: [A, B] => no errors')

  t.false(isRoot(mock([
    {}
  ])), 'joiningQuestions: [{}]')
  t.true(isRoot.errors.length > 0, 'joiningQuestions: [{}] => errors')

  t.false(isRoot(mock([
    { type: 'input', lableMisspelled: 'uh oh' }
  ])), 'missing label')
  t.true(isRoot.errors.length > 0, 'missing label: array item requires type and label')

  t.false(isRoot(mock([
    { typo: 'input', label: 'Ko wai koe?' }
  ])), 'missing label')
  t.true(isRoot.errors.length > 0, 'missing type: array item requires type and label')

  // test enum: [input, textarea]

  t.false(isRoot(mock([
    { type: 'radio', label: 'Hello, world!' }
  ])), 'unallowed type')

  t.true(isRoot.errors.length > 0, 'unallowed type: type must be input or textarea')

  server.close()
})
