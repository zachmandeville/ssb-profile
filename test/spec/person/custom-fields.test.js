const test = require('tape')
const CRUT = require('ssb-crut-authors')
const groupPersonSpec = require('../../../spec').person.group
const adminPersonSpec = require('../../../spec').person.admin
const Server = require('../../test-bot')

const groupId = '%VeNj9x/ZtI87TtSvtOEyw8G3gTrtWDe5OR9tDTAuLrg=.cloaked'

test('profile/person/group customFields (spec)', t => {
  t.plan(20)
  const mock = (value) => {
    return {
      type: 'profile/person',
      customFields: value,
      recps: [groupId],
      tangles: {
        profile: { root: null, previous: null }
      }
    }
  }

  let i = 0
  const generateTimestamp = () => {
    return (Date.now() + (i += 10)).toString()
  }

  const server = Server()
  const { isRoot } = new CRUT(server, groupPersonSpec)

  const mockTest = (value, isError = false) => {
    const timestamp = generateTimestamp()
    const field = {
      [timestamp]: value
    }

    if (isError) {
      t.false(isRoot(mock(field)), `isRoot(${value}) => false`)
      t.notEquals(isRoot.errors, null, `customFields: ${value} => returns errors`)
      return
    }

    t.true(isRoot(mock(field)), `isRoot ${value}`)
    t.equals(isRoot.errors, null, `customFields: ${value} => no errors`)
  }

  const mockFailingTest = (value) => mockTest(value, true)

  // text: a string as the value
  mockTest('Hometown')

  // multiple response: the value is an array of strings
  mockTest(['Bachelor of science'])

  // select: choose from a list of options
  mockTest(['Employed', 'Unemployed', 'Self-emplyed', 'Student'])

  // date:
  mockTest({ type: 'date', value: '2022-XX-24' }) // NOTE: how do we validate this when it will just say its a date

  // checkbox: true/false value
  mockTest(false)
  mockTest(true)

  // delete: can clear the value
  mockTest(null)

  // failing test for empty customField
  mockFailingTest({})

  // failing test for additional field
  mockFailingTest({
    um: 'Not supposed to be here'
  })

  // incorrect edtf date format
  mockFailingTest({
    type: 'date',
    value: '2022-XX-24-1'
  })

  server.close()
})

test('profile/person/admin customFields (spec)', t => {
  t.plan(16)
  const mock = (value) => {
    return {
      type: 'profile/person/admin',
      customFields: value,
      recps: [groupId],
      tangles: {
        profile: { root: null, previous: null }
      }
    }
  }

  let i = 0
  const generateTimestamp = () => {
    return (Date.now() + (i += 10)).toString()
  }

  const server = Server()
  const { isRoot } = new CRUT(server, adminPersonSpec)

  const mockTest = (value, isError = false) => {
    const timestamp = generateTimestamp()
    const field = {
      [timestamp]: value
    }

    if (isError) {
      t.false(isRoot(mock(field)), `isRoot(${value}) => false`)
      t.notEquals(isRoot.errors, null, `customFields: ${value} => returns errors`)
      return
    }

    t.true(isRoot(mock(field)), `isRoot ${value}`)
    t.equals(isRoot.errors, null, `customFields: ${value} => no errors`)
  }

  const mockFailingTest = (value) => mockTest(value, true)

  // text: a string as the value
  mockTest('Hometown')

  // multiple response: the value is an array of strings
  mockTest(['Bachelor of science'])

  // select: choose from a list of options
  mockTest(['Employed', 'Unemployed', 'Self-emplyed', 'Student'])

  // checkbox: true/false value
  mockTest(false)
  mockTest(true)

  // delete: can clear the value
  mockTest(null)

  // failing test for empty customField
  mockFailingTest({})

  // failing test for additional field
  mockFailingTest({
    um: 'Not supposed to be here'
  })

  server.close()
})
