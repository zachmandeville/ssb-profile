const test = require('tape')
const fixLinkCreateArgs = require('../../lib/fix-link-create-args')

test('fixLinkCreateArgs', t => {
  const profile = '%Uy7CoXJY0Ywe9Lj1EqbSjpeGYVgS4xntAxO6OfqB31k=.sha256'
  const parentGroupId = '%qBzOO6JC8OkvwmZA9UzAfSMWGVxXtv7BhaHDAlM+/so=.cloaked'
  const groupId = '%AfSMWGqBzA9UzOO6JC8OkvwmZVxXtv7BhaHDAlM+/so=.cloaked'

  // /////////
  // this is the legacy signature which we are supporting
  // ////////

  let args = fixLinkCreateArgs({ profile, allowPublic: true, parentGroupId })

  t.equal(args.childId, profile, 'returns profile as childId')
  t.deepEqual(
    args.opts,
    {
      groupId: undefined,
      allowPublic: true,
      parentGroupId
    },
    'fixLinkCreateArgs ({ profile, allowPublic, parentGroupId })'
  )

  args = fixLinkCreateArgs({ profile }, { allowPublic: true, parentGroupId })
  t.equal(args.childId, profile, 'returns profile as childId')
  t.deepEqual(
    args.opts,
    {
      groupId: undefined,
      allowPublic: true,
      parentGroupId
    },
    'fixLinkCreateArgs({ profile }, { allowPublic, parentGroupId })'
  )

  // /////////
  // this is the modern signature
  // ////////

  // for a subgroup
  args = fixLinkCreateArgs(profile, { groupId, parentGroupId })
  t.equal(args.childId, profile, 'returns profile as childId')
  t.deepEqual(
    args.opts,
    {
      groupId,
      parentGroupId
    }
  )

  t.end()
})
